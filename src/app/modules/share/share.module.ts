import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { NgModule } from '@angular/core';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { FormsModule } from '@angular/forms';

/**
 * Modulo que contendra contenido que se comparte en la aplicacion
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@NgModule({
  declarations: [
    HeaderComponent,
    SearchBarComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    SearchBarComponent
  ]
})
export class ShareModule { }
