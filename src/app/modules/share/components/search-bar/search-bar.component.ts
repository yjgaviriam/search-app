import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SearchResultsService } from 'src/app/components/search-results/services/search-results.service';

/**
 * Componente compartido con la barra de busqueda de la aplicacion
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.sass']
})
export class SearchBarComponent {

  /**
   * Estilos para el input de busqueda de acuerdo al componente padre
   */
  @Input()
  public classBar: string;

  /**
   * Estilos para la imagen de acuerdo al componente padre
   */
  @Input()
  public classImg: string;

  /**
   * Campo de busqueda
   */
  @Input()
  public searchField: string;

  /**
   * Constructor de la clase
   *
   * @author Jhonier Gaviria M. - Apr. 06-2021
   * @version 0.1.0
   *
   * @param router Controlador de rutas
   * @param searchResultsService Servicio para consultar la informacion
   */
  constructor(
    private router: Router,
    public searchResultsService: SearchResultsService
  ) { }

  /**
   * Redirige al componente de busqueda de resultados
   *
   * @author Jhonier Gaviria M. - Apr. 07-2021
   * @version 0.1.0
   */
  public searchResults(): void {
    if (this.searchField.trim().length > 0) {
      this.router.navigate(['results'], {
        queryParams: {
          q: this.searchField,
          page: this.searchResultsService.paginator.currentPage,
          resultsPerPage: this.searchResultsService.paginator.resultsPerPage
        }
      });
    }
  }
}
