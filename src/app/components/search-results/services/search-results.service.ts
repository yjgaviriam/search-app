import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPost } from '../interfaces/post.interface';
import { Observable } from 'rxjs';
import { IApiResponse } from '../interfaces/api-response.interface';
import { Paginator } from '../entities/paginator.entity';

/**
 * Servicio para obtener y procesar la data de los post
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@Injectable({
  providedIn: 'root'
})
export class SearchResultsService {

  /**
   * Contiene la informacion para construir el paginador
   */
  public paginator: Paginator;

  /**
   * Listado de post filtrados
   */
  public posts: IPost[];

  /**
   * Constructor de la clase
   *
   * @author Jhonier Gaviria M. - Apr. 06-2021
   * @version 0.1.0
   *
   * @param httpClient Servicio para realizar peticiones http
   */
  constructor(private httpClient: HttpClient) {
    this.paginator = new Paginator(1, 0, 2, -1, 10, 0);
  }

  /**
   * Obtiene los posts filtrados
   *
   * @author Jhonier Gaviria M. - Apr. 06-2021
   * @version 0.1.0
   *
   * @param tag Filtro de busqueda
   * @param page Pagina a consultar
   * @param resultsPerPage Cantidad de resultados a traer
   *
   * @returns Los posts filtrados
   */
  public getPosts(tag: string, page: number, resultsPerPage: number): Observable<IApiResponse> {

    // Cabecera necesaria para la peticion
    const headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'app-id': environment.appId
    });

    // Parametros de busqueda
    const params: HttpParams = new HttpParams()
      .set('page', String(page))
      .set('limit', String(resultsPerPage));

    return this.httpClient.get<IApiResponse>(`${environment.apiUrl}tag/${tag}/post`, { headers, params });
  }
}
