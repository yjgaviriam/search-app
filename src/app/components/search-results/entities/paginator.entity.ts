/**
 * Modelo con la estructura del paginador
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
export class Paginator {

  /**
   * Pagina actual del paginador
   */
  public currentPage: number;

  /**
   * Siguiente pagina a consultar
   */
  public nextPage: number;

  /**
   * Indica el ultimo resultado de la pagina actual
   */
  public offset: number;

  /**
   * Pagina anterior del paginador
   */
   public previousPage: number;

   /**
    * Cantidad de resultados que trae por pagina
    */
   public resultsPerPage: number;

   /**
    * Cantidad de resultados obtenidos
    */
   public totalResults: number;

   /**
    * Constructor de la clase
    *
    * @author Jhonier Gaviria M. - Apr. 07-2021
    * @version 0.1.0
    *
    * @param currentPage Pagina actual del paginador
    * @param offset Indica el ultimo resultado de la pagina actual
    * @param nextPage Siguiente pagina a consultar
    * @param previousPage Pagina anterior del paginador
    * @param resultsPerPage Cantidad de resultados que trae por pagina
    * @param totalResults Cantidad de resultados obtenidos
    */
   constructor(currentPage: number, offset: number, nextPage: number, previousPage: number, resultsPerPage: number, totalResults: number) {
    this.currentPage = currentPage;
    this.offset = offset;
    this.nextPage = ((resultsPerPage + offset) < totalResults) ? nextPage : -1;
    this.previousPage = (previousPage > 0 && totalResults !== 0) ? previousPage : -1;
    this.resultsPerPage = resultsPerPage;
    this.totalResults = totalResults;
   }
}
