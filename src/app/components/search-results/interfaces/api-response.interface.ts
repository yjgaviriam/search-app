import { IPost } from './post.interface';

/**
 * Interfaz con la estructura de la respuesta del ws de posts
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
export interface IApiResponse {

    /**
     * Listado de posts
     */
    data: IPost[];

    /**
     * Cantidad de registros por pagina
     */
    limit: number;

    /**
     * Ultimo registro de la pagina
     */
    offset: number;

    /**
     * Pagina consultada
     */
    page: number;

    /**
     * Cantidad de posts
     */
    total: number;
}
