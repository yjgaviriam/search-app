/**
 * Interfaz con la estructura de un propietario de un post
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
export interface IOwner {

    /**
     * Identificador del propietario
     */
    id: string;

    /**
     * Enlace de la foto del propietario
     */
    picture: string;

    /**
     * Primer nombre del propietario
     */
    firstName: string;

    /**
     * Correo electronico del propietario
     */
    email: string;

    /**
     * Prefijo en que se llama al propietario
     */
    title: string;

    /**
     * Apellido del propietario
     */
    lastName: string;
}
