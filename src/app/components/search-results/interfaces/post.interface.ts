import { IOwner } from './owner.interface';

/**
 * Interfaz con la estructura de un post
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
export interface IPost {

    /**
     * Propietario del post
     */
    owner: IOwner;

    /**
     * Identificador del post
     */
    id: string;

    /**
     * Enlace de la imagen del post
     */
    image: string;

    /**
     * Fecha de publicacion del post
     */
    publishDate: string;

    /**
     * Informacion del post
     */
    text: string;

    /**
     * Etiquetas del tema del post
     */
    tags: string[];

    /**
     * Enlace al post
     */
    link: string;

    /**
     * Numero de likes del post
     */
    likes: number;
}
