import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchResultsRoutingModule } from './search-results-routing.module';
import { SearchResultsComponent } from './components/search-results.component';
import { ShareModule } from 'src/app/modules/share/share.module';
import { ResultComponent } from './components/components/result/result.component';
import { FormsModule } from '@angular/forms';
import { PaintWordDirective } from './directives/paint-word.directive';

/**
 * Modulo de resultados de busqueda
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@NgModule({
  declarations: [
    PaintWordDirective,
    SearchResultsComponent,
    ResultComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ShareModule,
    SearchResultsRoutingModule
  ]
})
export class SearchResultsModule { }
