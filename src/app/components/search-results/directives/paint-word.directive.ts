import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

/**
 * Se encarga de pintar la palabra encontrada en un elemento
 *
 * @author Jhonier Gaviria M. - Apr. 07-2021
 * @version 0.1.0
 */
@Directive({
  selector: '[appPaintWord]'
})
export class PaintWordDirective implements AfterViewInit {

  /**
   * Palabra que se desea pintar
   */
  @Input()
  public appPaintWord: string;

  /**
   * Constructor de la clase
   *
   * @author Jhonier Gaviria M. - Apr. 07-2021
   * @version 0.1.0
   *
   * @param el Acceso al elemento conectado
   */
  constructor(private el: ElementRef) { }

  /**
   * @see https://angular.io/guide/lifecycle-hooks
   */
  public ngAfterViewInit(): void {
    this.el.nativeElement.innerHTML = this.el.nativeElement.innerHTML.replaceAll(this.appPaintWord,
      `<span style="color: red;">${this.appPaintWord}</span>`);
  }
}
