import { Component, Input } from '@angular/core';
import { IPost } from '../../../interfaces/post.interface';

/**
 * Componente encargado de renderizar un post
 *
 * @author Jhonier Gaviria M. - Apr. 07-2021
 * @version 0.1.0
 */
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass']
})
export class ResultComponent {

  /**
   * Instancia de un post para ser representado
   */
  @Input()
  public post: IPost;

  /**
   * Constructor de la clase
   *
   * @author Jhonier Gaviria M. - Apr. 07-2021
   * @version 0.1.0
   */
  constructor() { }
}
