import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { IApiResponse } from '../interfaces/api-response.interface';
import { Paginator } from '../entities/paginator.entity';
import { SearchResultsService } from '../services/search-results.service';

/**
 * Componente que muestra la ventana de resultados encontrados
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.sass']
})
export class SearchResultsComponent implements OnInit {

  /**
   * Campo de busqueda
   */
  public searchField: string;

  /**
   * Indica si ya se finalizo la peticion
   */
  public wsRequestWasComplete: boolean;

  /**
   * Constructor de la clase
   *
   * @author Jhonier Gaviria M. - Apr. 07-2021
   * @version 0.1.0
   *
   * @param route Controla la navegacion de la ruta actual
   * @param searchResultsService Servicio para consultar la informacion
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public searchResultsService: SearchResultsService
  ) {
    this.wsRequestWasComplete = false;
  }

  /**
   * @see https://angular.io/guide/lifecycle-hooks
   */
  public ngOnInit(): void {
    // Obtenemos el parametro de busqueda
    this.route.queryParams.subscribe(({ q, page, resultsPerPage }) => {
      this.searchField = q;
      this.searchResultsService.paginator.resultsPerPage = resultsPerPage;
      this.getPosts(page);
    });
  }

  /**
   * Obtiene los posts de acuerdo a los criterios de busqueda
   *
   * @author Jhonier Gaviria M. - Apr. 06-2021
   * @version 0.1.0
   *
   * @param page Pagina a consultar por la informacion
   */
  public getPosts(page: number): void {

    // Marcamos que se esta realizando una peticion
    this.wsRequestWasComplete = false;

    // Reiniciamos el arreglo
    this.searchResultsService.posts = [];

    // Vamos al servicio a realizar la peticion
    this.searchResultsService.getPosts(this.searchField, page, this.searchResultsService.paginator.resultsPerPage)
      .subscribe((result: IApiResponse) => {

        // Marcamos que ya fue respondida la solicitud
        this.wsRequestWasComplete = true;

        // Obtenemos los resultados
        this.searchResultsService.posts = result.data;

        // Configuramos la informacion del paginador
        this.searchResultsService.paginator = new Paginator(result.page, result.offset, (result.page + 1),
          (result.page - 1), Number(this.searchResultsService.paginator.resultsPerPage), result.total);
      });
  }

  /**
   * Realiza el cambio de pagina para consultar la informacion nueva
   *
   * @author Jhonier Gaviria M. - Apr. 06-2021
   * @version 0.1.0
   *
   * @param page Pagina a consultar por la informacion
   */
  public changeFilters(page: number): void {
    this.router.navigate([], { relativeTo: this.route, queryParams: {
      page,
      resultsPerPage: this.searchResultsService.paginator.resultsPerPage
    }, queryParamsHandling: 'merge' });
  }
}
