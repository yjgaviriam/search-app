import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchResultsComponent } from './components/search-results.component';

/**
 * Rutas para los resultados del buscador
 */
const routes: Routes = [
  { path: 'results', component: SearchResultsComponent },
  { path: '', pathMatch: 'full', redirectTo: 'results' }
];

/**
 * Modulo encargado de gestionar el enrutamiento de los resultados (lazy-loading)
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchResultsRoutingModule { }
