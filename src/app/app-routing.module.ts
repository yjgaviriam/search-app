import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

/**
 * Rutas de la aplicacion
 */
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'results', loadChildren: () => import('./components/search-results/search-results.module').then(m => m.SearchResultsModule) }
];

/**
 * Modulo encargado de gestionar el enrutamiento de la aplicacion
 *
 * @author Jhonier Gaviria M. - Apr. 06-2021
 * @version 0.1.0
 */
@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
